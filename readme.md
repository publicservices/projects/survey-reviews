# Survey Reviews

This is a frontend and Python API for a web form that'll post the submissions to an Airtable spreadsheet.

## Development

Setup the local dev environment:

```bash
cd survey-reviews
python3 -m venv venv
. venv/bin/activate
pip install -r requirements.txt
```

When the venv (virtual environment) is activated, run the local server with:

```bash
export FLASK_APP=api.py
flask run
```

## Deployment to production

API and frontend are being deployed by gitlab-ci.

The pipeline jobs are defined in the `/gitlab-ci.yml` file.

### API

The API is deployed to `heroku`.

### Frontend

The Frontend is deployed to `gitlab pages`.

## Data

The questions for the survey are in the json file inside the `/data` folder.

## API

- `GET /survey`, to obtain a list of 30 questions, to be taken as a survey. The first ten questions are hardcoded and the last twenty are random
- `POST /survey`, to submit the 20 answers of a survey
