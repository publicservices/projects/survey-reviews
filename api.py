import os
from flask import Flask, json, jsonify, request
from flask_cors import CORS, cross_origin
import random
import requests

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

@app.route('/')
@cross_origin()
def hello_world():
    return {
        "survey_endpoint": "/survey",
        "surveys_endpoint": "/surveys"
    }

@app.route('/surveys', methods=['GET'])
@cross_origin()
def list_surveys():
    path = "data/"
    list_of_files = {}
    for filename in os.listdir(path):
        fileId = os.path.splitext(filename)[0]
        list_of_files[fileId] = "/survey/"+fileId
    return jsonify(list_of_files)

@app.route('/survey/<survey_id>', methods=['GET'])
@cross_origin()
def create_survey(survey_id):

    if not survey_id:
        return jsonify({
            "usage": "/survey/<survey_id>",
            "all_surveys": "/surveys"
        })

    try:
        data_file = open("data/{}.json".format(survey_id), "r")
    except FileNotFoundError:
        return jsonify({
            "message": "Survey does not exist",
            "usage": "/survey/<survey_id>"
        })
    
    if data_file.mode == 'r':
        allQuestions = json.load(data_file)
        # reviewIdsForFirstTenQuestions = [1, 4, 5, 8, 9, 17, 19, 20, 21, 22,]
        randomQuestions = random.choices(allQuestions[10:], k=20)
        questions = allQuestions[0:10] + randomQuestions
        return jsonify({
            "id": survey_id,
            "title": survey_id,
            "description": "Welcome to the survey {}".format(survey_id),
            "questions": questions
        })


@app.route('/survey/<survey_id>', methods=['POST'])
@cross_origin()
def accept_survey_answer(survey_id):
    content = request.get_json(silent=True)

    headers = {
        "Authorization": "Bearer keyhoAWnYKENNCjZm",
        "Content-Type": "application/json",
    }
    response = requests.post(
        "https://api.airtable.com/v0/appXQYHyJZ9l94OCM/Table1",
        headers=headers,
        json=content,
    )

    return jsonify({
        "message": "Your answers to the survey '{}' have been saved. Thank you!".format(survey_id),
        "data": content,
    })

